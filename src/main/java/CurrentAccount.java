public class CurrentAccount extends Account {

    private double overdrawnLimit;

    public CurrentAccount(int theAccountNumber, int thePIN, double theAvailableBalance, double theTotalBalance) {
        super(theAccountNumber, thePIN, theAvailableBalance, theTotalBalance);
        overdrawnLimit=10000;
    }

    public CurrentAccount(int theAccountNumber, int thePIN, double theAvailableBalance, double theTotalBalance, double overdrawnLimit) {
        super(theAccountNumber, thePIN, theAvailableBalance, theTotalBalance);
        this.overdrawnLimit = overdrawnLimit;
    }

    public double getOverdrawnLimit() {
        return overdrawnLimit;
    }

    public void setOverdrawnLimit(double overdrawnLimit) {
        this.overdrawnLimit = overdrawnLimit;
    }
}
