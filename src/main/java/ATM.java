// ATM.java
// Represents an automated teller machine


import javax.swing.*;
import java.util.Timer;
import java.util.TimerTask;

public class ATM {
    //    private boolean userAuthenticated; // whether user is authenticated

    private static ATM atm;
    private int currentAccountNumber; // current user's account number
    private Screen screen; // ATM's screen
    private Keypad keypad; // ATM's keypad
    private CashDispenser cashDispenser; // ATM's cash dispenser
    private DepositSlot depositSlot; // ATM's deposit slot
    private BankDatabase bankDatabase; // account information database
    private JFrame frame;
    private java.util.Timer timer;

    // constants corresponding to main menu options
    private static final int BALANCE_INQUIRY = 1;
    private static final int WITHDRAWAL = 2;
    private static final int DEPOSIT = 3;
    private static final int TRANSFER = 4;
    private static final int EXIT = 5;

    // no-argument ATM constructor initializes instance variables
    public ATM() {
//        userAuthenticated = false; // user is  not authenticated to start
        currentAccountNumber = 0; // no current account number to start
        screen = new Screen(this); // create screen
        keypad = new Keypad(); // create keypad
        cashDispenser = new CashDispenser(); // create cash dispenser
        depositSlot = new DepositSlot(); // create deposit slot
        bankDatabase = new BankDatabase(); // create acct info database
        timer = new Timer();

        // singleton object
        atm = this;
    } // end no-argument ATM constructor


    public void start() {
        // setup the ATM Window for screen
        frame = new JFrame("Screen");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Holland Bank ATM 1.0");
        frame.setContentPane(screen.getMainPanel());
        frame.pack();
        frame.setVisible(true);
    }


    public int getCurrentAccountNumber() {
        return currentAccountNumber;
    }

    public void setCurrentAccountNumber(int currentAccountNumber) {
        this.currentAccountNumber = currentAccountNumber;
    }

    public Screen getScreen() {
        return screen;
    }

    public Keypad getKeypad() {
        return keypad;
    }

    public CashDispenser getCashDispenser() {
        return cashDispenser;
    }

    public DepositSlot getDepositSlot() {
        return depositSlot;
    }

    public BankDatabase getBankDatabase() {
        return bankDatabase;
    }

    public JFrame getFrame() {
        return frame;
    }

    public void endSession() {
        atm.setCurrentAccountNumber(0);
        screen.loadTransactionCompleteMessage("Thank you for using Holland Bank");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                screen.returnToLogin();
            }
        },9000L);

    }

    public Timer getTimer() {
        return timer;
    }

    public static ATM getInstance() {
        return atm;
    }

    // start ATM
    //  @Deprecated
//    public void run() {
//        // display main menu with login button
//

//    } // end method authenticateUser

    // display the main menu and peform transactions
//    private void performTransactions() {
//        // local variable to store transaction currently being processed
//        Transaction currentTransaction = null;
//
//        boolean userExited = false; // user has not chosen to exit
//
//        // loop while user has not chosen option to exit system
//        while (!userExited) {
//            // show main menu and get user selection
//            int mainMenuSelection = displayMainMenu();
//
//            // decide how to proceed based on user's menu selection
//            switch (mainMenuSelection) {
//                // user chose to perform one of three transaction types
//                case BALANCE_INQUIRY:
//                case WITHDRAWAL:
//                case DEPOSIT:
//                case TRANSFER:
//
//                    // initialize as new object of chosen type
//                    currentTransaction =
//                            createTransaction(mainMenuSelection);
//                    if (mainMenuSelection == 3) {
//                        screen.displayMessageLine("Sorry! The deposit function is currently disabled, please contact bank staff for further information");
//                    } else {
//                        currentTransaction.execute(); // execute transaction
//                    }
//
//                    break;
//
//                case EXIT: // user chose to terminate session
//                    screen.displayMessageLine("\nExiting the system...");
//                    userExited = true; // this ATM session should end
//                    break;
//                default: // user did not enter an integer from 1-4
//                    screen.displayMessageLine(
//                            "\nYou did not enter a valid selection. Try again.");
//                    break;
//            } // end switch
//        } // end while
//    } // end method performTransactions

    // display the main menu and return an input selection
//    private int displayMainMenu() {
//        screen.displayMessageLine("\nMain menu:");
//        screen.displayMessageLine("1 - View my balance");
//        screen.displayMessageLine("2 - Withdraw cash");
//        screen.displayMessageLine("3 - Deposit funds");
//        screen.displayMessageLine("4 - Transfer funds");
//        screen.displayMessageLine("5 - Exit\n");
//        screen.displayMessage("Enter a choice: ");
//        return keypad.getInput(); // return user's selection
//    } // end method displayMainMenu

    // return object of specified Transaction subclass
//    private Transaction createTransaction(int type) {
//        Transaction temp = null; // temporary Transaction variable
//
//        // determine which type of Transaction to create
//        switch (type) {
//            case BALANCE_INQUIRY: // create new BalanceInquiry transaction
//                temp = new BalanceInquiry(
//                        currentAccountNumber, screen, bankDatabase);
//                break;
//            case WITHDRAWAL: // create new Withdrawal transaction
//                temp = new Withdrawal(currentAccountNumber, screen,
//                        bankDatabase, keypad, cashDispenser);
//                break;
//            case DEPOSIT: // create new Deposit transaction
//                temp = new Deposit(currentAccountNumber, screen,
//                        bankDatabase, keypad, depositSlot);
//                break;
//            case TRANSFER:
//                temp = new Transfer(currentAccountNumber, screen,
//                        bankDatabase, keypad);
//
//                break;
//        } // end switch
//
//        return temp; // return the newly created object
//    } // end method createTransaction
} // end class ATM


/**************************************************************************
 * (C) Copyright 1992-2007 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/