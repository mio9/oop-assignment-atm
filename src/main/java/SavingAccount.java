public class SavingAccount extends Account {

    private double interestRate;

    public SavingAccount(int theAccountNumber, int thePIN, double theAvailableBalance, double theTotalBalance) {
        super(theAccountNumber, thePIN, theAvailableBalance, theTotalBalance);
        this.interestRate=0.001;
    }

    public SavingAccount(int theAccountNumber, int thePIN, double theAvailableBalance, double theTotalBalance, double interestRate) {
        super(theAccountNumber, thePIN, theAvailableBalance, theTotalBalance);
        this.interestRate = interestRate;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }
}
