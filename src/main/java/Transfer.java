import javax.swing.*;

/**
 * Represents a Transfer transaction
 */
public class Transfer extends Transaction {

    private Account destAccount;
    private double amount;

    // Constructor of Transfer Transaction
    public Transfer(ATM atm, int destAccountNumber, double amount) {
        super(atm);
        this.destAccount=atm.getBankDatabase().getAccount(destAccountNumber);
        this.amount=amount;
    }

    @Override
    public Status execute() {

        Account thisAccount = getAtm().getBankDatabase().getAccount(getAtm().getCurrentAccountNumber());
        if (destAccount==null||getAtm().getCurrentAccountNumber()==destAccount.getAccountNumber()){
            return Status.TRANSFER_INVALID_ACCOUNT;
        }
        // check if transfer amount is more than balance
        if (thisAccount instanceof CurrentAccount ? amount > thisAccount.getAvailableBalance() + ((CurrentAccount) thisAccount).getOverdrawnLimit() : amount > thisAccount.getAvailableBalance()) {
            return Status.TRANSFER_AMOUNT_MORE_THAN_BALANCE;
        } else {
            int confirmation = JOptionPane.showConfirmDialog(getAtm().getFrame(),"HK$"+amount+" will be transferred to "+destAccount.getAccountNumber()+", Confirm the transfer ?","Confirm transfer",JOptionPane.OK_CANCEL_OPTION);
            if (confirmation==0){
                getAtm().getBankDatabase().getAccount(getAtm().getCurrentAccountNumber()).debit(amount);
                destAccount.creditNow(amount);
                return Status.SUCCESS;
            } else {
                return Status.CANCEL;
            }

        }


    }

//    private Account promptAccount() {
//        getScreen().displayMessage("Please enter the account you want to transfer to: ");
//        int destAccountNumber = keypad.getInput();
//        destAccount = getBankDatabase().getAccount(destAccountNumber);
//        if (destAccount == null) {
//            getScreen().displayMessageLine("Please input a valid account number");
//        } else if (destAccountNumber == getAccountNumber()){
//            getScreen().displayMessageLine("You cannot transfer to your own account");
//        }
//        return destAccount;
//    }
//
//    private double promptAmount() {
//        getScreen().displayMessage("Please enter the amount you want to transfer: ");
//        double amount = keypad.getDoubleInput();
//        Account thisAccount=getBankDatabase().getAccount(getAccountNumber());
//        if (thisAccount instanceof CurrentAccount ? amount > thisAccount.getAvailableBalance() + ((CurrentAccount) thisAccount).getOverdrawnLimit() : amount > thisAccount.getAvailableBalance()) {
//            getScreen().displayMessageLine("You cannot transfer more than your balance!");
//            getScreen().displayMessageLine("Your available balance: HK$" + getBankDatabase().getAccount(getAccountNumber()).getAvailableBalance());
//            return 0;
//        } else return amount;
//    }
}
