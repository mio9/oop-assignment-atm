// Transaction.java
// Abstract superclass Transaction represents an ATM transaction

public abstract class Transaction {
    enum Status{
        SUCCESS,
        CANCEL,
        WITHDRAW_AMOUNT_NOT_SUPPORTED,
        WITHDRAW_AMOUNT_MORE_THAN_BALANCE,
        TRANSFER_INVALID_ACCOUNT,
        TRANSFER_AMOUNT_MORE_THAN_BALANCE,
        WITHDRAW_DISPENSER_NOT_ENOUGH_CASH
    }
    private ATM atm; //the atm object

    public Transaction(ATM atm) {
        this.atm = atm;
    }

    public ATM getAtm() {
        return atm;
    }

    // perform the transaction (overridden by each subclass)
    abstract public Status execute();
} // end class Transaction


/**************************************************************************
 * (C) Copyright 1992-2007 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/