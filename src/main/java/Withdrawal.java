// Withdrawal.java
// Represents a withdrawal ATM transaction

import javax.swing.*;

public class Withdrawal extends Transaction {
    private int amount; // amount to withdraw

    public Withdrawal(ATM atm, int amount) {
        super(atm);
        this.amount = amount;
    }


// perform transaction

    public Transaction.Status execute() {
        double availableBalance = getAtm().getBankDatabase().getAvailableBalance(getAtm().getCurrentAccountNumber()); // amount available for withdrawal

        // get references to bank database and screen
        BankDatabase bankDatabase = getAtm().getBankDatabase();
        Screen screen = getAtm().getScreen();
        Account thisAccount = bankDatabase.getAccount(getAtm().getCurrentAccountNumber());

        if (amount % 100 == 0 && amount > 0) {
            // check whether the user has enough money in the account
            if (thisAccount instanceof CurrentAccount ? amount <= availableBalance + ((CurrentAccount) thisAccount).getOverdrawnLimit() : amount <= thisAccount.getAvailableBalance()) {
                // check whether the cash dispenser has enough money
                if (getAtm().getCashDispenser().isSufficientCashAvailable(amount)) {
                    int confirmation = JOptionPane.showConfirmDialog(getAtm().getFrame(), "HK$" + amount + " will be withdrawn, confirm the withdrawal?", "Confirmation", JOptionPane.OK_CANCEL_OPTION);
                    // yes, confirm
                    if (confirmation == 0) {
                        bankDatabase.debit(getAtm().getCurrentAccountNumber(), amount);
                        getAtm().getCashDispenser().dispenseCash(amount); // dispense cash
//                    cashDispensed = true; // cash was dispensed
                        return Status.SUCCESS;
                    } else {
                        return Status.CANCEL;
                    }
//                    System.out.println("confirmation = " + confirmation);
                    // update the account involved to reflect withdrawal

                    // instruct user to take cash

                } // end if
                else // cash dispenser does not have enough cash
                    return Status.WITHDRAW_DISPENSER_NOT_ENOUGH_CASH;
            } // end if
            else // not enough money available in user's account
            {
                return Status.WITHDRAW_AMOUNT_MORE_THAN_BALANCE;
            } // end else
        } else {
            return Status.WITHDRAW_AMOUNT_NOT_SUPPORTED;
        }


    }


    // display a menu of withdrawal amounts and the option to cancel;
    // return the chosen amount or 0 if the user chooses to cancel

//    private int displayMenuOfAmounts() {
//        int userChoice = 0; // local variable to store return value
//
//        ScreenOld screen = getScreen(); // get screen reference
//
//        // array of amounts to correspond to menu numbers
//        int amounts[] = {0, 100, 200, 500, 1000, 2000};
//
//        // loop while no valid choice has been made
//        while (userChoice == 0) {
//            // display the menu
//            screen.displayMessageLine("\nWithdrawal Menu:");
//            screen.displayMessageLine("1 - $100");
//            screen.displayMessageLine("2 - $200");
//            screen.displayMessageLine("3 - $500");
//            screen.displayMessageLine("4 - $1000");
//            screen.displayMessageLine("5 - $2000");
//            screen.displayMessageLine("6 - Other..");
//            screen.displayMessageLine("7 - Cancel");
//            screen.displayMessage("\nChoose a withdrawal amount: ");
//
//            int input = keypad.getInput(); // get user input through keypad
//
//            // determine how to proceed based on the input value
//            switch (input) {
//                case 1: // if the user chose a withdrawal amount
//                case 2: // (i.e., chose option 1, 2, 3, 4 or 5), return the
//                case 3: // corresponding amount from amounts array
//                case 4:
//                case 5:
//                    userChoice = amounts[input]; // save user's choice
//                    break;
//                case 6:
//                    userChoice = promptAmount();
//                    break;
//                case CANCELED: // the user chose to cancel
//                    userChoice = CANCELED; // save user's choice
//                    break;
//                default: // the user did not enter a value from 1-6
//                    screen.displayMessageLine(
//                            "\nIvalid selection. Try again.");
//            } // end switch
//        } // end while
//
//        return userChoice; // return withdrawal amount or CANCELED
//    } // end method displayMenuOfAmounts


//    private int promptAmount() {
//        getScreen().displayMessageLine("");
//        getScreen().displayMessage("Please enter the amount you want to withdraw: ");
//        return keypad.getInput();
//    }
} // end class Withdrawal


/**************************************************************************
 * (C) Copyright 1992-2007 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/