import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Timer;
import java.util.TimerTask;

public class Screen {
    private JButton switchToLoginButton;
    private JButton switchToMainButton;
    private JPanel switchablePanel;
    private JPanel loginPanel;
    private JPanel mainMenuPanel;
    private JPanel mainPanel;
    private JPanel devSwitcherPanel;
    private JTextField loginAcField;
    private JPasswordField loginPwField;
    private JButton loginButton;
    private JButton balanceInquiryButton;
    private JButton withdrawButton;
    private JButton transferButton;
    private JButton depositButton;
    private JButton exitButton;
    private JButton switchToInquiryButton;
    private JButton switchToDepositButton;
    private JButton switchToWithdrawButton;
    private JButton switchToTransferButton;
    private JPanel inquiryPanel;
    private JPanel transferPanel;
    private JPanel withdrawlPanel;
    private JPanel depositPanel;
    private JLabel currentAccount;
    private JButton withdrawButton1;
    private JButton returnToMenuButton;
    private JLabel totalBalanceLabel;
    private JLabel availableBalanceLabel;
    private JLabel accountNumberLabel;
    private JPanel loadingPanel;
    private JLabel loadingLabel;
    private JTextField withdrawAmountField;
    private JButton withdrawButton2;
    private JPanel transactionCompletePanel;
    private JLabel transactionCompleteMessage;
    private JTextField transferAmountField;
    private JTextField transferTargetAccountField;
    private JButton execTransferButton;
    private JButton cancelButton;
    private JButton cancelButton1;
    private ATM atm;

    public Screen(ATM atm) {
        //setup loading page
        setupLoading();


        //development button array
        switchToMainButton.addActionListener(e -> switchTo(mainMenuPanel));
        switchToLoginButton.addActionListener(e -> switchTo(loginPanel));
        switchToInquiryButton.addActionListener(e -> switchTo(inquiryPanel));
        switchToDepositButton.addActionListener(e -> switchTo(loadingPanel));
        switchToTransferButton.addActionListener(e -> switchTo(transferPanel));
        switchToWithdrawButton.addActionListener(e -> switchTo(withdrawlPanel));

        //login form
        loginButton.addActionListener(e -> {
            //login
            if (!loginAcField.getText().isEmpty() && loginPwField.getPassword().length != 0 && atm.getBankDatabase().authenticateUser(Integer.parseInt(loginAcField.getText()), Integer.parseInt(String.valueOf(loginPwField.getPassword())))) {
                setDevCurrentAccountText(loginAcField.getText());
                atm.setCurrentAccountNumber(Integer.parseInt(loginAcField.getText()));
                load(mainMenuPanel);

            } else {
                JOptionPane.showMessageDialog(atm.getFrame(), "Invalid account / password, please try again!", "Unable to sign on", JOptionPane.WARNING_MESSAGE);
            }

        });
        //main menu
        exitButton.addActionListener(e -> {
            transactionCompleteMessage.setText("Please take your card");
            switchTo(transactionCompletePanel);
            System.out.println("Take card");
            atm.getTimer().schedule(new TimerTask() {
                @Override
                public void run() {
                    atm.endSession();
                    System.out.println("logout");
                }
            }, 6000L);

        });
        depositButton.addActionListener(e -> JOptionPane.showMessageDialog(atm.getFrame(), "Deposit function is not yet ready. Please try again later or contact our staff", "Function Not Ready", JOptionPane.INFORMATION_MESSAGE));
        transferButton.addActionListener(e -> {
            transferTargetAccountField.setText("");
            transferAmountField.setText("");
            switchTo(transferPanel);
        });
        withdrawButton.addActionListener(e -> {
            switchTo(withdrawlPanel);
            withdrawAmountField.setText("");
        });
        balanceInquiryButton.addActionListener(e -> {
            load(inquiryPanel);
            totalBalanceLabel.setText(String.valueOf(atm.getBankDatabase().getTotalBalance(atm.getCurrentAccountNumber())));
            availableBalanceLabel.setText(String.valueOf(atm.getBankDatabase().getAvailableBalance(atm.getCurrentAccountNumber())));
            accountNumberLabel.setText(String.valueOf(atm.getCurrentAccountNumber()));
        });

        //inquiry page
        returnToMenuButton.addActionListener(e -> switchTo(mainMenuPanel));

        withdrawButton1.addActionListener(e -> switchTo(withdrawlPanel));

        // withdraw page
        withdrawButton2.addActionListener(e -> {
            int withdrawAmount = 0;
            if (withdrawAmountField.getText().isEmpty()) {
                withdrawAmountField.setText("0");
            }
            try {
                withdrawAmount = Integer.parseInt(withdrawAmountField.getText());
            } catch (NumberFormatException e1) {
                JOptionPane.showMessageDialog(atm.getFrame(), "This ATM provides only HK$100 and HKD500 cash. Please correct your amount and proceed.", "Unable to withdraw", JOptionPane.WARNING_MESSAGE);
                withdrawAmountField.setText("");
                return;
            }
            Withdrawal withdrawalTransaction = new Withdrawal(atm, withdrawAmount);

            Transaction.Status status = withdrawalTransaction.execute();

            switch (status) {
                case SUCCESS:
                    load(transactionCompletePanel);
                    transactionCompleteMessage.setText("HK$" + withdrawAmount + " withdrawal is accepted, Please take your card");
                    System.out.println("take card");
                    atm.getTimer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            transactionCompleteMessage.setText("Please take your cash");
                            System.out.println("Take cast=h");
                            atm.getTimer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    atm.endSession();
                                    System.out.println("logout");
                                }
                            }, 6000L);
                        }
                    }, 10000L);
                    break;
                case WITHDRAW_AMOUNT_NOT_SUPPORTED:
                    JOptionPane.showMessageDialog(atm.getFrame(), "This ATM provides only HK$100 and HKD500 cash. Please correct your amount and proceed.", "Unable to withdraw", JOptionPane.WARNING_MESSAGE);
                    withdrawAmountField.setText("");
                    break;
                case WITHDRAW_AMOUNT_MORE_THAN_BALANCE:
                    JOptionPane.showMessageDialog(atm.getFrame(), "You may not withdraw more than your withdrawal allowance!", "Unable to withdraw", JOptionPane.WARNING_MESSAGE);
                    withdrawAmountField.setText("");
                    break;
                case WITHDRAW_DISPENSER_NOT_ENOUGH_CASH:
                    JOptionPane.showMessageDialog(atm.getFrame(), "This ATM is out of cash! Please contact our staff at 3746-0126", "Unable to withdraw", JOptionPane.ERROR_MESSAGE);
                    withdrawAmountField.setText("");
                    break;
                case CANCEL:
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + status);
            }

            // confirm
            // $400 withdrawal accepted, please take your card
            // enter
            // Please take your fucking cash
            //
        });
        withdrawAmountField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                withdrawAmountField.selectAll();
            }
        });
        cancelButton.addActionListener(e -> switchTo(mainMenuPanel));

        //transfer page
        execTransferButton.addActionListener(e -> {
//            System.out.println("transferAmountField = " + transferAmountField.getText());
//            System.out.println("transferTargetAccountField = " + transferTargetAccountField.getText());
            if (transferAmountField.getText().isEmpty()) {
                transferAmountField.setText("0");
            }
            if (transferTargetAccountField.getText().isEmpty()) {
                transferTargetAccountField.setText("0");
            }
            int targetAccountNo = Integer.parseInt(transferTargetAccountField.getText());
            double amount = Double.parseDouble(transferAmountField.getText());
            Transaction.Status status = new Transfer(atm, targetAccountNo, amount).execute();
            switch (status) {
                case SUCCESS:
                    load(transactionCompletePanel);
                    transactionCompleteMessage.setText("HK$" + amount + " transfer is accepted, Please take your card");
                    System.out.println("take card");
                    atm.getTimer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            atm.endSession();
                            System.out.println("logout");
                        }
                    }, 10000L);
                    break;
                case TRANSFER_INVALID_ACCOUNT:
                    JOptionPane.showMessageDialog(atm.getFrame(), "Please enter a valid account number", "Unable to transfer", JOptionPane.WARNING_MESSAGE);
                    transferTargetAccountField.grabFocus();
                    transferTargetAccountField.selectAll();
                    break;
                case TRANSFER_AMOUNT_MORE_THAN_BALANCE:
                    JOptionPane.showMessageDialog(atm.getFrame(), "You may not transfer more than your withdrawal allowance!", "Unable to transfer", JOptionPane.WARNING_MESSAGE);
                    transferAmountField.grabFocus();
                    transferAmountField.selectAll();
                    break;
                case CANCEL:
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + status);
            }

        });
        cancelButton1.addActionListener(e -> switchTo(mainMenuPanel));
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void switchTo(JPanel targetPanel) {
        switchablePanel.removeAll();
        switchablePanel.add(targetPanel);
        switchablePanel.repaint();
        switchablePanel.revalidate();
    }

    /**
     * switchTo, but with simulated loading delay
     *
     * @param targetPanel
     */
    public void load(JPanel targetPanel) {
        switchTo(loadingPanel);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                switchTo(targetPanel);
            }
        }, (long) (1500 + (Math.random() * 5000) % 4000));
    }

    public void setDevCurrentAccountText(String accountNum) {
        this.currentAccount.setText(accountNum);
    }

    private void setupLoading() {
        String[] loadingTxt = {"Please wait.", "Please wait..", "Please wait..."};

        TimerTask update = new TimerTask() {
            int count = 0;

            @Override
            public void run() {
                loadingLabel.setText(loadingTxt[count % 3]);
                count++;
            }
        };
        Timer t = new Timer("Timer");
        t.scheduleAtFixedRate(update, 1000L, 1000L);
    }

    public void loadTransactionCompleteMessage(String message) {
        load(transactionCompletePanel);
        transactionCompleteMessage.setText(message);
    }

    public void returnToLogin() {
        loginAcField.setText("");
        loginPwField.setText("");
        switchTo(loginPanel);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(2, 1, new Insets(20, 20, 20, 20), 10, 10));
        mainPanel.setPreferredSize(new Dimension(500, 400));
        switchablePanel = new JPanel();
        switchablePanel.setLayout(new CardLayout(0, 0));
        mainPanel.add(switchablePanel, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        loginPanel = new JPanel();
        loginPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
        loginPanel.setInheritsPopupMenu(false);
        loginPanel.setPreferredSize(new Dimension(192, 50));
        switchablePanel.add(loginPanel, "Card1");
        loginAcField = new JTextField();
        loginPanel.add(loginAcField, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, 30), null, 0, false));
        loginPwField = new JPasswordField();
        loginPanel.add(loginPwField, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, 30), new Dimension(-1, 30), 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Account Number");
        loginPanel.add(label1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("PIN");
        loginPanel.add(label2, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        loginButton = new JButton();
        loginButton.setText("Login");
        loginPanel.add(loginButton, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        Font label3Font = this.$$$getFont$$$(null, -1, 18, label3.getFont());
        if (label3Font != null) label3.setFont(label3Font);
        label3.setText("Welcome to Holland Bank. Please insert card or Login");
        loginPanel.add(label3, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        mainMenuPanel = new JPanel();
        mainMenuPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        switchablePanel.add(mainMenuPanel, "Card2");
        balanceInquiryButton = new JButton();
        balanceInquiryButton.setOpaque(true);
        balanceInquiryButton.setText("Balance Inquiry");
        balanceInquiryButton.setVerticalAlignment(0);
        mainMenuPanel.add(balanceInquiryButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        withdrawButton = new JButton();
        withdrawButton.setText("Withdraw");
        mainMenuPanel.add(withdrawButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        transferButton = new JButton();
        transferButton.setText("Transfer");
        mainMenuPanel.add(transferButton, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        exitButton = new JButton();
        exitButton.setText("Exit");
        mainMenuPanel.add(exitButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        depositButton = new JButton();
        depositButton.setText("Deposit");
        mainMenuPanel.add(depositButton, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        inquiryPanel = new JPanel();
        inquiryPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
        switchablePanel.add(inquiryPanel, "Card3");
        totalBalanceLabel = new JLabel();
        Font totalBalanceLabelFont = this.$$$getFont$$$(null, -1, 14, totalBalanceLabel.getFont());
        if (totalBalanceLabelFont != null) totalBalanceLabel.setFont(totalBalanceLabelFont);
        totalBalanceLabel.setText("------");
        inquiryPanel.add(totalBalanceLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        Font label4Font = this.$$$getFont$$$(null, -1, 14, label4.getFont());
        if (label4Font != null) label4.setFont(label4Font);
        label4.setText("Available Balance");
        inquiryPanel.add(label4, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        availableBalanceLabel = new JLabel();
        Font availableBalanceLabelFont = this.$$$getFont$$$(null, -1, 14, availableBalanceLabel.getFont());
        if (availableBalanceLabelFont != null) availableBalanceLabel.setFont(availableBalanceLabelFont);
        availableBalanceLabel.setText("------");
        inquiryPanel.add(availableBalanceLabel, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        withdrawButton1 = new JButton();
        withdrawButton1.setText("Withdraw...");
        inquiryPanel.add(withdrawButton1, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        returnToMenuButton = new JButton();
        returnToMenuButton.setText("Return to Menu");
        inquiryPanel.add(returnToMenuButton, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        Font label5Font = this.$$$getFont$$$(null, -1, 14, label5.getFont());
        if (label5Font != null) label5.setFont(label5Font);
        label5.setText("Total Balance:");
        inquiryPanel.add(label5, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        Font label6Font = this.$$$getFont$$$(null, -1, 14, label6.getFont());
        if (label6Font != null) label6.setFont(label6Font);
        label6.setText("Account Number:");
        inquiryPanel.add(label6, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        accountNumberLabel = new JLabel();
        Font accountNumberLabelFont = this.$$$getFont$$$(null, -1, 14, accountNumberLabel.getFont());
        if (accountNumberLabelFont != null) accountNumberLabel.setFont(accountNumberLabelFont);
        accountNumberLabel.setText("------");
        inquiryPanel.add(accountNumberLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        transferPanel = new JPanel();
        transferPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1));
        switchablePanel.add(transferPanel, "Card4");
        transferAmountField = new JTextField();
        Font transferAmountFieldFont = this.$$$getFont$$$("Consolas", -1, 16, transferAmountField.getFont());
        if (transferAmountFieldFont != null) transferAmountField.setFont(transferAmountFieldFont);
        transferPanel.add(transferAmountField, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(-1, 30), new Dimension(150, 30), null, 0, false));
        final JLabel label7 = new JLabel();
        label7.setText("Transfer to...");
        transferPanel.add(label7, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        label8.setText("Amount");
        transferPanel.add(label8, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        transferTargetAccountField = new JTextField();
        transferPanel.add(transferTargetAccountField, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(-1, 30), new Dimension(150, -1), null, 0, false));
        execTransferButton = new JButton();
        execTransferButton.setText("Transfer");
        transferPanel.add(execTransferButton, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cancelButton1 = new JButton();
        cancelButton1.setText("Cancel");
        transferPanel.add(cancelButton1, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        withdrawlPanel = new JPanel();
        withdrawlPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1));
        switchablePanel.add(withdrawlPanel, "Card5");
        final JLabel label9 = new JLabel();
        Font label9Font = this.$$$getFont$$$(null, -1, 14, label9.getFont());
        if (label9Font != null) label9.setFont(label9Font);
        label9.setText("Please enter the amount to withdraw ");
        withdrawlPanel.add(label9, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        withdrawAmountField = new JTextField();
        Font withdrawAmountFieldFont = this.$$$getFont$$$("Consolas", Font.BOLD, 16, withdrawAmountField.getFont());
        if (withdrawAmountFieldFont != null) withdrawAmountField.setFont(withdrawAmountFieldFont);
        withdrawAmountField.setText("0");
        withdrawlPanel.add(withdrawAmountField, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(-1, 30), new Dimension(150, -1), null, 0, false));
        withdrawButton2 = new JButton();
        withdrawButton2.setText("Withdraw");
        withdrawlPanel.add(withdrawButton2, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label10 = new JLabel();
        label10.setText("HK$");
        withdrawlPanel.add(label10, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cancelButton = new JButton();
        cancelButton.setText("Cancel");
        withdrawlPanel.add(cancelButton, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        depositPanel = new JPanel();
        depositPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        switchablePanel.add(depositPanel, "Card6");
        loadingPanel = new JPanel();
        loadingPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        switchablePanel.add(loadingPanel, "Card7");
        loadingLabel = new JLabel();
        Font loadingLabelFont = this.$$$getFont$$$(null, -1, 18, loadingLabel.getFont());
        if (loadingLabelFont != null) loadingLabel.setFont(loadingLabelFont);
        loadingLabel.setText("Please wait");
        loadingPanel.add(loadingLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        transactionCompletePanel = new JPanel();
        transactionCompletePanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        switchablePanel.add(transactionCompletePanel, "Card8");
        transactionCompleteMessage = new JLabel();
        Font transactionCompleteMessageFont = this.$$$getFont$$$(null, -1, 16, transactionCompleteMessage.getFont());
        if (transactionCompleteMessageFont != null) transactionCompleteMessage.setFont(transactionCompleteMessageFont);
        transactionCompleteMessage.setText("e");
        transactionCompletePanel.add(transactionCompleteMessage, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        devSwitcherPanel = new JPanel();
        devSwitcherPanel.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(3, 3, new Insets(0, 0, 0, 0), -1, -1));
        devSwitcherPanel.setEnabled(false);
        devSwitcherPanel.setVisible(false);
        mainPanel.add(devSwitcherPanel, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, new Dimension(-1, 100), new Dimension(-1, 100), 0, false));
        switchToLoginButton = new JButton();
        switchToLoginButton.setText("Login");
        devSwitcherPanel.add(switchToLoginButton, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        switchToMainButton = new JButton();
        switchToMainButton.setText("Main Menu");
        devSwitcherPanel.add(switchToMainButton, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        switchToWithdrawButton = new JButton();
        switchToWithdrawButton.setText("switch to withdraw");
        devSwitcherPanel.add(switchToWithdrawButton, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        switchToInquiryButton = new JButton();
        switchToInquiryButton.setText("switch to Inquiry");
        devSwitcherPanel.add(switchToInquiryButton, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        switchToDepositButton = new JButton();
        switchToDepositButton.setText("switch to deposit");
        devSwitcherPanel.add(switchToDepositButton, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        switchToTransferButton = new JButton();
        switchToTransferButton.setText("switch to transfer");
        devSwitcherPanel.add(switchToTransferButton, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        currentAccount = new JLabel();
        currentAccount.setText("CurrentAccount");
        devSwitcherPanel.add(currentAccount, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        label1.setLabelFor(loginAcField);
        label2.setLabelFor(loginPwField);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
